"""Gluecode that is run on provisioners to provide cli-advertised functions."""
import os
from queue import Empty
from queue import SimpleQueue

from cki_lib.misc import safe_popen
from cki_lib.misc import tempfile_from_string
from twisted.internet import reactor
from twisted.internet import task
from twisted.internet.threads import deferToThread

from upt.logger import COLORS
from upt.logger import LOGGER
from upt.plumbing.end_conditions import ProvEndConds
from upt.plumbing.format import ProvisionData
from upt.restraint.wrapper.misc import attempt_reactor_stop


class ProvisionerGlue:
    """Glue that uses provisioners to provide user functions."""

    def __init__(self, provisioner_data, **kwargs):
        """Create the object."""
        self.provisioner_data = provisioner_data
        self.provisioners = provisioner_data.provisioners

        self.kwargs = kwargs
        self.request_file = kwargs['request_file']

        self.results_queue = SimpleQueue()

    def update_provisioners(self, number_of_resource_groups):
        """Update provisioners with results from queues produced in threads."""
        retcodes = []
        while len(retcodes) < number_of_resource_groups:
            # Block until we get a resource ready
            try:
                i, j, retcode, new_rg = self.results_queue.get(timeout=2)
            except Empty:
                continue

            self.provisioners[i].rgs[j] = new_rg
            self.provisioners[i].update_provisioning_request(new_rg)
            retcodes.append(retcode)

        # Update provisioning request, so recipe_ids are reflected after waiting!
        self.provisioner_data.serialize2file(self.request_file)

        if any(retcodes):
            # Provisioning timed out or ran out of retries.
            raise RuntimeError('Provisioning failed!')

    def process_resource_group(self, provisioner, resource_group, i, j, number_of_resource_groups,
                               **kwargs):
        # pylint: disable=too-many-arguments,too-many-positional-arguments
        """Wait on provisioning, update data and possibly run restraint runner."""
        # We're in a separate thread, it's fine to block and wait for provisioning.
        end_conditions = ProvEndConds(resource_group, provisioner.is_provisioned,
                                      provisioner.reprovision_aborted,
                                      provisioner.evaluate_wait_time,
                                      provisioner.evaluate_confirm_time,
                                      provisioner.provisioning_watchdog_update_time)
        # Wait on a single resource_group.
        resource_group.wait(end_conditions, provisioner.set_reservation_duration,
                            provisioner.release_rg,
                            **kwargs)
        # Fill-in details of provisioning for this resource_group.
        provisioner.update_provisioning_request(resource_group)

        if self.kwargs['pipeline']:
            self.invoke_restraint_runner(resource_group, i, number_of_resource_groups)
        else:
            self.results_queue.put((i, j, end_conditions.retcode.value, resource_group))

    def invoke_restraint_runner(self, resource_group, i, number_of_resource_groups):
        """Invoke restraint runner on a certain resource group of provisioner indexed by i."""
        if not any(resource_group.recipeset.hosts):
            raise RuntimeError('No host to run testing on!')

        prov = ProvisionData()
        temp_prov = prov.get_provisioner(self.provisioners[i].name, create=True)
        temp_prov.rgs = [resource_group]
        with tempfile_from_string(b'') as fname:
            prov.instance_no = number_of_resource_groups + 1
            prov.serialize2file(fname)
            # This will block until testing is finished. restraint runner does not update/write
            # resource groups. Only changes from UPT provisioning will be reflected.

            args = [os.environ.get('VENV_PY3', 'python3'), '-m', 'upt.restraint.wrapper', 'test',
                    '-i', fname] + self.kwargs['pipeline'].split(' ')

            LOGGER.debug('running restraint runner with: %s', str(args))
            _, _, _ = safe_popen(args)

    def do_wait(self):
        """Use provisioner method to wait for resources to be ready."""
        number_of_resource_groups = 0
        for i, provisioner in enumerate(self.provisioners):
            for j, resource_group in enumerate(provisioner.rgs):
                def_wait_and_opt_test = deferToThread(
                    self.process_resource_group,
                    provisioner,
                    resource_group,
                    i,
                    j,
                    number_of_resource_groups,
                    **self.kwargs,
                )
                def_wait_and_opt_test.addErrback(print)
                number_of_resource_groups += 1

        # Restraint runner has its own cleanup
        if not self.kwargs['pipeline']:
            # Update provisioning request, so resource_ids are reflected
            self.update_provisioners(number_of_resource_groups)

        # We're done, stop reactor.
        attempt_reactor_stop(reactor)

    def run_provisioners(self):
        """Provision all resources."""
        if self.kwargs.get('dry_run', False):
            LOGGER.printc('dry run: skipping provisioning resource(s)',
                          color=COLORS.MAGENTA)
            return

        for provisioner in self.provisioners:
            provisioner.provision(**self.kwargs)

        defer_do_wait = task.deferLater(reactor, 0, self.do_wait)
        # On error, print error.
        defer_do_wait.addErrback(print)
        reactor.run()
