"""Testing Farm provisioner."""
import base64
import os
import pathlib
import re
from urllib.parse import urlsplit

from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.asymmetric import rsa
import requests
from ruamel.yaml.scalarstring import PreservedScalarString
from tf_requests import get_auto_compose

from upt.logger import LOGGER
from upt.misc import fixup_or_delete_tasks_without_fetch
from upt.plumbing.interface import ProvisionerCore
from upt.restraint.file import RestraintJob

headers = {'Content-Type': 'application/json'}
DEFAULT_TF_RESERVATION_DURATION = 1440


def builder_release_tag():
    """Determine stream and release url values from automotive config url."""
    parts = urlsplit(os.environ['AUTOMOTIVE_CONFIGURATION_URL']).path.strip('/').split('/')
    stream = parts[1]
    release = parts[2]
    return f'{stream}/{release}'


def get_automotive_compose() -> str:
    """Get automotive compose information for request."""
    release = builder_release_tag() \
        if ('rhivos' in os.environ['AUTOMOTIVE_TOOLCHAIN_URL']) else 'nightly'
    config = {"RELEASE_NAME": release,
              "ARCH": os.environ['ARCH_CONFIG'],
              "HW_TARGET": os.environ.get('TF_HARDWARE', 'ridesx4'),
              "webserver_releases": os.environ['AUTOMOTIVE_TOOLCHAIN_URL'],
              "IMAGE_NAME": 'qa',  # default
              "IMAGE_TYPE": 'regular'  # default
              }
    compose = get_auto_compose.calculate_auto_compose(config)
    return compose[1]


def get_environment(pub_key):
    """Get request key:environment info."""
    if os.environ.get('AUTOMOTIVE_TOOLCHAIN_URL'):
        compose_val = get_automotive_compose()
    else:
        compose_val = os.environ['OS']

    return {
        'arch': os.environ['ARCH_CONFIG'],
        'os': {
            'compose': compose_val
        },
        'pool': os.environ.get('TF_POOL'),
        'artifacts': [],
        'variables': {
            'TF_RESERVATION_DURATION': DEFAULT_TF_RESERVATION_DURATION
        },
        'secrets': {
            'TF_RESERVATION_AUTHORIZED_KEYS_BASE64': pub_key
        }
    }


def get_data(pub_key):
    """Get data for building the testing farm request."""
    return {
        'api_key': os.environ['TF_API_TOKEN'],
        'test': {
            'fmf': {
                'url': 'https://gitlab.com/testing-farm/tests',
                'ref': 'main',
                'name': '/testing-farm/reserve'
            }
        },
        'environments': [
            get_environment(pub_key)
        ]
    }


class TestingFarmException(Exception):
    """Testing Farm Error."""


class TestingFarm(ProvisionerCore):
    """Testing Farm provisioner."""

    def __init__(self, dict_data=None):
        """Construct instances."""
        super().__init__(dict_data=dict_data)
        self.evaluate_confirm_time = 0
        self.provisioned_hosts = []
        self.tf_guestname = ''
        self.rsa_key = rsa.generate_private_key(
            public_exponent=65537,
            key_size=2048,
            backend=default_backend()
        )
        self.ssh_directory = '/tmp'

    def provision(self, **kwargs):
        """Provision all hosts in all resource groups."""
        if (rgs_count := len(self.rgs)) != 1:
            raise Exception(f'only one resource group supported, got {rgs_count}')
        resource_group = self.rgs[0]
        resource_group.resource_id = '1'
        recipe_set = resource_group.recipeset
        restraint_job = RestraintJob.create_from_string(recipe_set.restraint_xml)
        restraint_recipes = restraint_job.get_all_recipes()
        assert len(restraint_recipes) == len(
            recipe_set.hosts), "Invalid data; must have 1 host per 1 recipe"

        host = resource_group.recipeset.hosts[0]
        public_key = self.get_public_key()
        data = get_data(public_key)
        try:
            response = requests.post(f"{os.environ['TF_API_URL']}/requests", headers=headers,
                                     json=data, timeout=5)

        except requests.exceptions.RequestException as err:
            msg = f'An error occurred while making the request: {err}'
            raise TestingFarmException(msg) from err

        if response.status_code == 200:
            r_json = response.json()
            host.recipe_id = self.host_recipe_id_monotonic.get()
            restraint_recipes[0].id = str(host.recipe_id)
            host.misc['resource_id'] = r_json['id']
            LOGGER.info('Request created successfully, guest_name: %s',
                        host.misc.get('resource_id'))
        else:
            msg = f'Response unexpected: Code {response.status_code}, Text {response.text}'
            raise TestingFarmException(msg)

        # Write private key
        private_key_path = pathlib.Path(self.ssh_directory, host.misc.get('resource_id'))
        self.write_private_key(private_key_path)

        #  Adding ssh options
        resource_group.ssh_opts = f'-i {private_key_path}'

        fixup_or_delete_tasks_without_fetch(restraint_job)
        recipe_set.restraint_xml = PreservedScalarString(str(restraint_job))

    def get_public_key(self):
        """Get a public key encoded in base 64 needed by TestingFarm."""
        ssh_public_key = self.rsa_key.public_key().public_bytes(
            encoding=serialization.Encoding.OpenSSH,
            format=serialization.PublicFormat.OpenSSH
        )
        encoded_ssh_public_key = base64.b64encode(ssh_public_key)
        return encoded_ssh_public_key.decode('utf-8')

    def write_private_key(self, path):
        """Write the private key."""
        path.parent.mkdir(parents=True, exist_ok=True)
        pem = self.rsa_key.private_bytes(
            encoding=serialization.Encoding.PEM,
            format=serialization.PrivateFormat.TraditionalOpenSSL,
            encryption_algorithm=serialization.NoEncryption()
        )
        path.write_text(pem.decode('utf-8'), encoding='utf-8')
        path.chmod(0o600)
        LOGGER.debug('Private key successfully generated in %s', str(path.resolve()))

    def is_provisioned(self, resource_group):
        """Check if resource group is finished provisioning."""
        provisioned, _ = self.get_provisioning_state(resource_group)
        return provisioned

    def get_provisioning_state(self, resource_group):
        """
        Get current state of provisioning.

        NOTE: this method is not a required part of the provisioner interface.
        """
        provisioned = False
        erred = []

        host = resource_group.recipeset.hosts[0]
        resource_id = host.misc.get('resource_id')
        try:
            response = requests.get(f"{os.environ['TF_API_URL']}/requests/{resource_id}",
                                    timeout=5)

        except requests.exceptions.RequestException as err:
            msg = f'An error occurred while making the request: {err}'
            raise TestingFarmException(msg) from err

        if response.status_code == 200:
            r_json = response.json()
            state = r_json['state']

            if state == 'running':
                pipeline_url = f'{r_json["run"]["artifacts"]}/pipeline.log'
                if (hostname := self.get_ip_from_pipeline(pipeline_url)):
                    LOGGER.debug('Host %s successfully provisioned', hostname)
                    host.hostname = hostname
                    provisioned = True
                else:
                    LOGGER.debug('Unable to find the IP')
                    erred.append(host)
            elif state == 'error':
                raise TestingFarmException('The status of the provision is error')
            else:
                LOGGER.debug('Problem with provision :( currently the status is %s', state)
                erred.append(host)

        else:
            msg = f'Response unexpected: Code {response.status_code}, Text {response.text}'
            raise TestingFarmException(msg)

        return provisioned, erred

    @staticmethod
    def get_ip_from_pipeline(pipeline_url):
        """Get the ip from the pipeline."""
        try:
            LOGGER.info('Getting the pipeline log from: %s', pipeline_url)
            response = requests.get(pipeline_url, timeout=5)

        except requests.exceptions.RequestException as err:
            msg = f'An error occurred while making the request: {err}'
            raise TestingFarmException(msg) from err

        if response.status_code == 200:
            pipeline_log = response.text
            search = re.search(r'Guest is ready.*root@([\d\w\.-]+)', pipeline_log)

            if search and 'execute task #1' in pipeline_log:
                return search.group(1)

            return None
        msg = f'Response unexpected: Code {response.status_code}, Text {response.text}'
        raise TestingFarmException(msg)

    def reprovision_aborted(self, resource_group):
        """Provision a resource again, if provisioning failed."""
        # nothing to do here as provisioning cannot fail

    def get_resource_ids(self):
        """Return identifiers of resources provisioned during script run."""
        raise Exception('not implemented, as this is not used anywhere')

    def heartbeat(self, resource_group, recipe_ids_dead):
        """Check if resource is OK (provisioned, not broken/aborted)."""
        # nothing to do here

    def release_rg(self, resource_group):
        """Release resource provisioned in resource group."""
        for host in resource_group.recipeset.hosts:
            LOGGER.debug('"TestFarm id" : %s sending release', host.misc.get('resource_id'))
            response = requests.delete(f"{os.environ['TF_API_URL']}/requests/"
                                       f"{host.misc.get('resource_id')}",
                                       timeout=5,
                                       json={"api_key": os.environ['TF_API_TOKEN']})
            LOGGER.debug('release_response : %s', response.status_code)

    def update_provisioning_request(self, resource_group):
        """Ensure that request file has up-to-date info after provisioning."""
        # nothing to do here
