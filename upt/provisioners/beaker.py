"""Simple & extensible Beaker provisioner."""

import pathlib
import re
import shlex
import subprocess
import sys
import time
import xml.etree.ElementTree as ET

from cki_lib import misc
from cki_lib.retrying import retrying_on_exception
from ruamel.yaml.scalarstring import PreservedScalarString

from upt import const
from upt.logger import COLORS
from upt.logger import LOGGER
from upt.misc import fixup_or_delete_tasks_without_fetch
from upt.misc import recipe_installing_or_waiting
from upt.misc import recipe_not_provisioned
from upt.misc import reservesys_task_problem
from upt.misc import sanitize_link
from upt.plumbing.interface import ProvisionerCore
from upt.plumbing.objects import Host
from upt.restraint.file import RestraintJob
from upt.restraint.file import RestraintRecipeSet
from upt.restraint.file import RestraintTask


class InternalServerError(Exception):
    """Beaker responded with 500 Internal Server Error."""


def is_internal_server_error(output):
    """Check if a message indicating Internal Server Error is present."""
    if '500 Internal Server Error' in output:
        return True
    return False


@retrying_on_exception(InternalServerError)
def submit_job(fname, dryrun=False):
    """Submit a job to Beaker."""
    dryrun_opt = '--dryrun' if dryrun else ''
    cmd = f'bkr job-submit {dryrun_opt} {fname}'

    stdout, stderr, returncode = misc.safe_popen(shlex.split(cmd), stdout=subprocess.PIPE,
                                                 stderr=subprocess.PIPE)

    if is_internal_server_error(stdout + stderr):
        raise InternalServerError('Submitting job failed!')

    # Ignore a specific warning.
    for line in stderr.splitlines():
        if 'WARNING: job xml validation failed: Element task has extra content' not in line:
            sys.stderr.write(line + '\n')

    if returncode:
        print(stdout)
        LOGGER.error('submitting job failed!')

        return []

    # process stdout using a regex to find J:XXXX resource ids
    resource_ids = re.findall(const.PATTERN_JOBIDS, stdout)

    # let user know what was submitted
    print(f'Submitted: {" ".join(resource_ids)}')

    return resource_ids


@retrying_on_exception(InternalServerError)
def clone(task_spec):
    """Re-provision a task_spec like J: or RS:."""
    cmd = f'bkr job-clone {task_spec}'

    stdout, stderr, returncode = misc.safe_popen(shlex.split(cmd),
                                                 stdout=subprocess.PIPE,
                                                 stderr=subprocess.PIPE)
    if is_internal_server_error(stdout + stderr):
        raise InternalServerError(f'Cloning {task_spec} failed!')

    if returncode:
        print(stdout)
        raise RuntimeError(f'cloning {task_spec} failed!')

    # process stdout using a regex to find J:XXXX resource ids
    resource_ids = re.findall(const.PATTERN_JOBIDS, stdout)

    return resource_ids


def cancel(task_spec):
    """Cancel a task_spec like J: or RS:."""
    cmd = f'bkr job-cancel {task_spec}'

    stdout, _, returncode = misc.safe_popen(shlex.split(cmd),
                                            stdout=subprocess.PIPE)
    print(stdout.strip())
    if returncode:
        LOGGER.printc('Failed to release resources(s)', color=COLORS.RED)


class Beaker(ProvisionerCore):
    # pylint: disable=too-many-public-methods
    """Datastructure describing Beaker provisioner."""

    # a set of recipe_ids that aborted
    _warned_rsetids = set()

    # A list of log files that are ignored.
    output_files_ignore = [
        'anaconda.log',
        'boot.log',
        'DeBug',
        'dnf.librepo.log',
        'hawkey.log',
        'ifcfg.log',
        'journal.xml',
        'ks.cfg',
        'lvm.log',
        'messages',
        'packaging.log',
        'program.log',
        'storage.log',
        'sys.log',
        'systemd_journal.log',
        'zipl.conf',
    ]

    @classmethod
    def set_reservation_duration(cls, host):
        """Ensure host remains provisioned for host.duration."""
        result = subprocess.run(
            ['bkr', 'watchdog-extend', f'R:{host.recipe_id}', '--by', str(host.duration)],
            check=False, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        if result.returncode:
            LOGGER.error('Unable to set reservation duration: %s\n%s',
                         host.recipe_id, result.stdout)

    @classmethod
    @retrying_on_exception(RuntimeError)
    def get_bkr_results(cls, resource_id):
        """Get Beaker job results for resource_id."""
        cmd = f'bkr job-results {resource_id}'

        stdout, _, returncode = misc.safe_popen(shlex.split(cmd),
                                                stdout=subprocess.PIPE)
        if returncode:
            print(stdout)
            raise RuntimeError('bkr job-results failed!')

        return stdout

    @classmethod
    def get_dead_resource_info(cls, rgs, result_restraint_recipes, recipe_ids_dead, method):
        """Try to determine what caused the resource to become unresponsive."""
        # Exclude dead systems that are already reported.
        for restraint_recipe in filter(lambda x: int(x.id) not in recipe_ids_dead,
                                       result_restraint_recipes):
            host = cls.find_host_object(rgs, int(restraint_recipe.id))
            # Select reservesys tasks only and run detection method (check for EWD hits or panics).
            for restraint_task in filter(lambda task: 'reservesys' in task.name,
                                         restraint_recipe.tasks):
                for restraint_result in restraint_task.results:
                    method(host, restraint_result, restraint_recipe, recipe_ids_dead)

    @classmethod
    def check_for_panic(cls, host, restraint_result, restraint_recipe, _):
        """Check if panic occurred on Beaker recipe."""
        if 'oops' in restraint_result.text.lower() or restraint_result.result == 'Panic':
            LOGGER.printc(f'#{restraint_recipe.id}: KERNEL PANIC!',
                          color=COLORS.RED)
            host.panicked = True

    @classmethod
    def check_for_ewd(cls, _, restraint_result, restraint_recipe, recipe_ids_dead):
        """Check if EWD was hit on a Beaker recipe."""
        if restraint_result.text == 'External Watchdog Expired':
            LOGGER.debug('#%s: found EWD in reservesys task!', restraint_recipe.id)
            # Add recipe_id to a set of recipe ids hit by EWD.
            recipe_ids_dead.add(int(restraint_recipe.id))

    def quirk_wait(self, resource_group):
        """Wait a while, then get Beaker results again."""
        delay = 60
        LOGGER.debug('* Caught Beaker quirk; waiting %d...', delay)
        time.sleep(delay)

        restraint_job = RestraintJob.create_from_scratch()
        _, restraint_job.recipesets, _ = self.get_provisioning_state(resource_group)
        return restraint_job.get_all_recipes()

    @classmethod
    def check_beaker_quirk(cls, erred_recipes):
        """Check if Beaker quirk occured."""
        for erred_rec in erred_recipes:
            if erred_rec.status == 'Aborted' and erred_rec.result == 'New':
                return True
            for task in erred_rec.tasks:
                if task.status == 'Aborted' and task.result == 'New':
                    return True

        return False

    def heartbeat(self, resource_group, recipe_ids_dead):
        """Detect EWD hits and panics on hosts in this resource_group."""
        restraint_job = RestraintJob.create_from_scratch()
        _, restraint_job.recipesets, _ = self.get_provisioning_state(resource_group)
        restraint_recipes = restraint_job.get_all_recipes()

        # Detect panics promptly at any cost.
        self.get_dead_resource_info([resource_group], restraint_recipes,
                                    recipe_ids_dead, self.check_for_panic)
        # Beaker quirk: transition of result: New -> Panic. Redownload data after 60 seconds.
        while self.check_beaker_quirk(restraint_recipes):
            restraint_recipes = self.quirk_wait(resource_group)

        # Check for EWD hits.
        self.get_dead_resource_info([resource_group], restraint_recipes,
                                    recipe_ids_dead, self.check_for_ewd)

    def warn_once_provisioning_issue(
        self,
        recipe_id,
        recipe_ids,
        rset_id,
        err_status,
        reservesys_err_status,
    ):
        # pylint: disable=too-many-arguments,too-many-positional-arguments
        """Warn once about an issue with rset_id provisioning."""
        if rset_id not in self._warned_rsetids:
            if err_status:
                msg = f'* RS:{rset_id} ({recipe_ids}): R:{recipe_id} {err_status}.'
            elif reservesys_err_status:
                msg = (f'* RS:{rset_id} ({recipe_ids}): '
                       f'found reservesys task that {reservesys_err_status}.')
            else:
                raise RuntimeError(
                    'unhandled condition; cannot determine what caused recipe to abort')

            print(msg)
            self._warned_rsetids.add(rset_id)

    def is_provisioned(self, resource_group):
        """Check if resource group is finished provisioning."""
        provisioned, _, _ = self.get_provisioning_state(resource_group)
        return provisioned

    def get_provisioning_state(self, resource_group):
        """Get info about provisioning.

        This checks every recipeSet in resource_group. The method pulls fresh
        data from Beaker. If any recipe of a recipeSet has an issue, or doesn't
        have reservesys task in a proper state, then we add recipeSet id to the
        set of 'erred' recipeSets. This ends-up ensuring that entire recipeSet
        is cloned, we don't assume bigger granularity. If that's required, then
        it's a bad decision on the part of who provides us the Beaker job xml;
        we have to respect synchronization domains and this is the simplest
        way.

        NOTE: this method is not a required part of the provisioner interface.
        Each provisioner implements and uses this method in its own way,
        although the method signature is very similar each time.
        """
        restraint_job = RestraintJob.create_from_string(
            self.get_bkr_results(resource_group.resource_id))
        restraint_recipesets = restraint_job.recipesets
        for restraint_recipeset in restraint_recipesets:
            restraint_recipes = restraint_recipeset.recipes
            # Start with no issues for this recipeSet.
            any_error = False
            rset_installing = False
            for restraint_recipe in restraint_recipes:
                # get all tasks restraint uses to reserve system
                restraint_tasks = [task for task in restraint_recipe.tasks
                                   if task.name == '/distribution/reservesys']

                # scan for abort in recipe status
                err_status = recipe_not_provisioned(restraint_recipe)
                # scan for abort in reservesys task
                reservesys_err_status = reservesys_task_problem(restraint_tasks)
                any_error = err_status or reservesys_err_status
                rset_installing |= recipe_installing_or_waiting(restraint_recipe)

                if any_error:
                    self.warn_once_provisioning_issue(restraint_recipe.id,
                                                      [rec.id for rec in restraint_recipes],
                                                      restraint_recipeset.id,
                                                      err_status,
                                                      reservesys_err_status)
                    # Add any recipeSet id that has issue. We will have to
                    # clone it. Don't check other recipes, there's no point.
                    resource_group.erred_rset_ids.add(int(restraint_recipeset.id))
                    break
                for restraint_task in restraint_tasks:
                    # We're iterating through tasks again. If the recipe is
                    # really in a good state, then it should have
                    # reservesys task with Running/New status/result.
                    if restraint_task.result != 'New' or restraint_task.status != 'Running':
                        LOGGER.debug('%i: %s %s %s',
                                     int(restraint_recipe.id),
                                     restraint_task.name,
                                     restraint_task.status,
                                     restraint_task.result)
                        any_error = True
                        break

            if not any_error and not rset_installing:
                # Add recipe that provisioned OK. The recipe has to belong to this rg.
                resource_group.provisioned_ok_rsets_ids.add(int(restraint_recipeset.id))

        # This is the ultimate condition to checking if everything was provisioned.
        provisioned = len(restraint_recipesets) == len(resource_group.provisioned_ok_rsets_ids)

        return provisioned, restraint_job.recipesets, resource_group.erred_rset_ids

    @staticmethod
    def delete_failed_hosts(recipeset):
        """Remove hosts that failed provisioning."""
        recipeset.hosts = list(filter(
            lambda host: host.recipe_id is not None and host.hostname != '', recipeset.hosts))

        # Delete recipes in restraint_xml that failed to provision
        restraint_job = RestraintJob.create_from_string(recipeset.restraint_xml)
        for restraint_recipeset in restraint_job.recipesets:
            restraint_recipeset.recipes = list(filter(lambda rec: rec.id,
                                                      restraint_recipeset.recipes))
        recipeset.restraint_xml = PreservedScalarString(str(restraint_job))

    def update_provisioning_request(self, resource_group):
        """Ensure that request file has up-to-date info from Beaker.

        Resources that were provisioned OK must have accurate recipe_id and hostname;
        the coresponding restraint_xml has to be updated.
        Tasks without fetch element have to be deleted, because restraint cannot run those.
        """
        restraint_job = RestraintJob.create_from_string(
            self.get_bkr_results(resource_group.resource_id)
        )
        recipeset = resource_group.recipeset
        restraint_recipeset = restraint_job.recipesets[0]
        recipeset.id = f"RS:{restraint_recipeset.id}"
        for i, (restraint_recipe, host) in enumerate(zip(
                restraint_recipeset.recipes, recipeset.hosts)):
            # scan for abort in recipe status
            err_status = recipe_not_provisioned(restraint_recipe)
            # scan for abort in reservesys task
            reservesys_err_status = reservesys_task_problem(restraint_recipe.tasks)
            any_error = err_status or reservesys_err_status
            system = restraint_recipe.system
            if not any_error and system:
                host.hostname = system
                host.recipe_id = int(restraint_recipe.id)

                # update restraint xml
                cur_restraint_job = RestraintJob.create_from_string(recipeset.restraint_xml)

                recipe2mod = cur_restraint_job.recipesets[0].recipes[i]
                recipe2mod.id = str(restraint_recipe.id)
                recipe2mod.job_id = resource_group.resource_id.lstrip('J:')

                fixup_or_delete_tasks_without_fetch(cur_restraint_job)

                recipeset.restraint_xml = PreservedScalarString(str(cur_restraint_job))
            else:
                host.recipe_id = None
                host.hostname = ''

                if not resource_group.erred_rset_ids:
                    LOGGER.printc('Error: a system went down shortly after provisioning '
                                  'was completed!', color=COLORS.RED)

        self.delete_failed_hosts(recipeset)

    def resource_group2xml(self, resource_group):
        """Read provisioner data resource group, create XML."""
        restraint_job = RestraintJob.create_from_string(resource_group.job)

        # reconstruct XML from legacy provisioning request
        new_recipeset = RestraintRecipeSet.create_from_string(
            self._finalize_recipeset(resource_group)
        )
        restraint_job.recipesets.append(new_recipeset)

        return self._finalize_recipes(restraint_job, resource_group.recipeset)

    def provision(self, **kwargs):
        """Entry point, provision resources according to provisioner_data."""
        xmls = [self.resource_group2xml(rg) for rg in self.rgs]

        all_resource_ids = []
        for i, xml in enumerate(xmls):
            # submit the job
            with misc.tempfile_from_string(xml.encode('utf-8')) as fname:
                # we submit by 1 resource_id (1 Beaker job)
                resource_id = submit_job(fname)[0]
                # save the resource id, so we can return it
                all_resource_ids.append(resource_id)
                # save resource ids of what we've provisioned
                self.rgs[i].resource_id = resource_id

        return all_resource_ids

    @classmethod
    def adjust_recipes(cls, restraint_job, recipeset):
        """Add reservesys tasks with duration, remove dummy command."""
        hosts_durations = []

        for host in recipeset.hosts:
            hosts_durations.append(host.duration)

        restraint_recipes = restraint_job.get_all_recipes()

        for j, restraint_recipe in enumerate(restraint_recipes):
            duration = hosts_durations[j] if hosts_durations[j] else \
                const.DEFAULT_RESERVESYS_DURATION
            # Log this, could be important.
            LOGGER.debug('recipe duration is: %i', duration)

            # disable the 01_dmesg_check to not impact the restraint tasks
            # that are executed while the system is reserved.
            reservesys = f"""<task name="/distribution/reservesys" role="None">
            <fetch url="{const.URL_RESERVESYS_TASK}"/><params>
            <param name="RESERVETIME" value="{duration}" />
            <param name="RSTRNT_DISABLED" value="01_dmesg_check" />
            </params></task>"""
            # add reservesys task
            restraint_recipe.tasks.append(RestraintTask.create_from_string(reservesys))

        return restraint_job

    def reprovision_aborted(self, resource_group):
        """Provision a resource again, if provisioning failed.

        This is a Beaker method that looks at provisioner resources per-recipe.
        Those recipes that failed provisioning (canceled/aborted) are cloned
        and the recipe_id is added to a list of recipe_ids that are not going
        to be cloned again. If we need to, then we clone a clone of the
        original recipe_id.
        """
        # Get all recipeSets that had any issue (aborted, ...) and were not reprovisioned
        not_reprovisioned_rset_ids = set(filter(lambda rset_id: rset_id not in
                                                resource_group.reprovisioned_rsets_ids,
                                                resource_group.erred_rset_ids))
        if not not_reprovisioned_rset_ids:
            # There's nothing to reprovision.
            return

        # Cancel recipeSets, we have to provision everything again!
        for rset_id in not_reprovisioned_rset_ids:
            cancel(f'RS:{rset_id}')

        # Keep information about what failed provisioning so we have the total list.
        resource_group.reprovisioned_rsets_ids = resource_group.reprovisioned_rsets_ids.union(
            not_reprovisioned_rset_ids)

        for rset_id in not_reprovisioned_rset_ids:
            resource_group.resource_id = clone(f'RS:{rset_id}')[0]
            # let user know what's happening
            print(f'* Cloned RS:{rset_id} as {resource_group.resource_id}')

    @classmethod
    def _finalize_recipeset(cls, resource_group):
        """Take recipeset template and create <recipeSet /> elements."""
        new_recipes = [host.recipe_fill for host in resource_group.recipeset.hosts]
        new_recipeset = f'<recipeSet priority="{resource_group.priority}">' + \
                        '\n'.join(new_recipes) + '</recipeSet>'

        return new_recipeset

    def _finalize_recipes(self, restraint_job, recipeset):
        """Add restraint reservesys and set harness."""
        restraint_job = self.adjust_recipes(restraint_job, recipeset)

        return str(restraint_job)

    def release_rg(self, resource_group):
        """Release all resources provisioned in a single resource group."""
        cancel(resource_group.resource_id)

    def exclude_hosts(self, hostnames):
        """Ensure that we do not provision specific hosts."""
        for host in self.find_objects(self.rgs, lambda x: x if isinstance(x, Host) else None):
            tree = ET.fromstring(host.recipe_fill)

            for host_requires in tree.findall('hostRequires'):
                if host_requires.get('force'):
                    # If we specifically want to run on a certain host, so be it.
                    continue

                for restricted in hostnames:
                    # Ensure hostname isn't appended twice.
                    if not [node for node in host_requires.findall('hostname')
                            if node.get('op') == '!=' and node.get('value') == restricted]:
                        hostname_node = ET.fromstring(f'<hostname op="!=" value="{restricted}" />')
                        host_requires.append(hostname_node)

            pretty_xml = ET.canonicalize(ET.tostring(tree, encoding='unicode'))
            host.recipe_fill = PreservedScalarString(pretty_xml)

    def get_resource_ids(self):
        """Return identifiers of resources provisioned during script run."""
        return [rg.resource_id for rg in self.rgs]

    @classmethod
    def get_links2logs(cls, recipe_id):
        """Get links to log files of recipe_id, except output_files_ignore."""
        stdout, _, returncode = misc.safe_popen(['bkr', 'job-logs', f'R:{recipe_id}'],
                                                stdout=subprocess.PIPE)
        if returncode:
            LOGGER.error('bkr job-logs issue')

        return list(filter(lambda line: f'{recipe_id}/logs/' in line and
                                        not any(x in line for x in cls.output_files_ignore),
                           stdout.splitlines()))

    @classmethod
    def download_logs(cls, output_dir, hosts, kernel_version=None):
        """Download logs of a hostnames into output_dir/hostname."""
        LOGGER.debug('Downloading Beaker logs...')
        downloaded_files = []
        for host in hosts:
            dir_path = pathlib.Path(output_dir, host.counter.path)
            dir_path.mkdir(parents=True, exist_ok=True)
            for link in cls.get_links2logs(host.recipe_id):
                path = sanitize_link(link, dir_path, kernel_version)
                downloaded_files.append((host.counter.path, path))

        return downloaded_files
