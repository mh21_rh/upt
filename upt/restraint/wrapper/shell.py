"""Generate restraint shell commands from provisioning request."""
import os
import tempfile

from upt import const
from upt.logger import LOGGER
from upt.misc import compute_tasks_duration
from upt.restraint.file import RestraintJob


class ShellWrap:
    # pylint: disable=too-many-instance-attributes
    """Generate restraint shell commands from provisioning request."""

    def __init__(self, resource_group, **kwargs):
        """Create the object."""
        # Is strict keycheck used for ssh?
        self.keycheck = kwargs['keycheck']
        # Number of restraint connection retries
        self.conn_retries = 15

        # pylint: disable=consider-using-with
        # Directory to temporarily store output
        self.tempdir = tempfile.TemporaryDirectory()
        fpath = self.tempdir.name

        # Restraint job xml path
        self.job_abspath = os.path.abspath(f'{fpath}/{const.RSTRNT_JOB_XML}')

        # Resource group for this instance
        self.resource_group = resource_group

        # RestraintJob that is being run
        self.run_restraint_job = RestraintJob.create_from_string(
            self.resource_group.recipeset.restraint_xml
        )

        # Update task from previous attempt
        self.remove_ended_sucessfully_tasks()

        # Update duration time
        self.update_host_durations()

        # Write job file
        self.run_restraint_job.write(self.job_abspath)

        # Commands to invoke restraint
        self.restraint_commands = self.get_commands()

        self.cleanup_done = False

    def __del__(self):
        """Clean temp output directory."""
        self.tempdir.cleanup()

    @staticmethod
    def update_duration(host, restraint_tasks):
        """Update how long should the host stay up."""
        # Add-up duration. Use 120%.
        host.duration = compute_tasks_duration(restraint_tasks)
        host.duration = (int(host.duration / 100 * 120) if host.duration
                         else const.DEFAULT_RESERVESYS_DURATION)
        # Save this info in debug log
        LOGGER.debug('* %i has %0.2fh length', host.recipe_id, host.duration / 3600)

    def update_host_durations(self):
        """Update host duration based on task durations."""
        restraint_recipes = self.run_restraint_job.get_all_recipes()
        for recipe, host in zip(restraint_recipes, self.rs_hosts()):
            # Update how long will the host run
            self.update_duration(host, recipe.tasks)

    def remove_ended_sucessfully_tasks(self):
        """Update XML without tasks were run OK."""
        restraint_recipes = self.run_restraint_job.get_all_recipes()
        for recipe, host in zip(restraint_recipes, self.rs_hosts()):
            if host.rerun_recipe_tasks_from_index is not None:
                valid_tasks = []
                for i, task in enumerate(recipe.tasks):
                    if i + 1 >= host.rerun_recipe_tasks_from_index:
                        valid_tasks.append(task)
                recipe.tasks = valid_tasks

    def rs_hosts(self):
        """Get hosts for the recipe set."""
        return list(filter(lambda host: host.recipe_id is not None,
                           self.resource_group.recipeset.hosts))

    def get_hosts_with_arguments(self):
        """Get host paramaters to be added to the console commands."""
        # Craft restraint command. Example for 1 recipe:
        # $ restraint --host 1=root@1.2.3.4 --job restraint.xml
        return [f'--host {host.recipe_id}=root@{host.hostname}' for host in self.rs_hosts()]

    def get_commands(self):
        """Get the command line used by restraint."""
        hosts = self.get_hosts_with_arguments()
        opts = f'-o PubkeyAcceptedKeyTypes=+ssh-rsa ' \
               f'-o HostKeyAlgorithms=+ssh-rsa ' \
               f'-o ServerAliveInterval=60 -o ServerAliveCountMax=5 ' \
               f'-o StrictHostKeyChecking={self.keycheck}' + \
               (' ' + self.resource_group.ssh_opts if self.resource_group.ssh_opts else '')
        # Prepare restraint command
        cmd = f'restraint --conn-retries {self.conn_retries} ' \
              f'--job {self.job_abspath} --rsh "ssh {opts}"'  f' {" ".join(hosts)} '

        return cmd
