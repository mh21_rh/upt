"""Actions to do on a specific test result."""
from upt.logger import LOGGER


class ActionOnResult:
    """Represent actions done on a specific task result."""

    basefmt = 'R:{recipe_id}     T:{task_id}     {statusresult} {sign} {testname}'
    # Example:
    #   Argument: kcidb_status
    #   Value:    one of "ERROR", "FAIL", "PASS", "DONE", "SKIP", "MISS"
    #   Effect:   the test in kcidb dumpfile will have this status/result set
    #
    #   Argument: lst_actions
    #   Value:    []
    #   Effect:   no methods run when conditions are satisfied
    #
    #   Argument: msg_string
    #   Value:    '(Just caught a specific status. Keep testing!)'
    #   Effect:   suffix of a message printed when conditions are satisfied
    #
    #   Argument: kwargs
    #   Value:    status='Running'
    #   Effect:   Conditions are satisfied when status field is 'Running'
    #
    # ActionOnResult([],
    #                '(Just caught a specific status. Keep testing!)',
    #                None,
    #                status='Running'),

    def __init__(self, kcidb_status, lst_actions, msg_string, **kwargs):
        """Create the object."""
        self.kwargs = kwargs
        self.lst_actions = lst_actions
        self.msg_string = self.basefmt + '\n' + msg_string + '\n'
        self.kcidb_status = kcidb_status

    def __str__(self):
        """Describe what conditions are set by kwargs for action to evaluate to True."""
        values = ' '.join([f'{k}={v}' for k, v in self.kwargs.items()])

        return values

    @classmethod
    def format_msg(cls, recipe_id, task_id, statusresult, sign, testname, base=basefmt):
        # pylint: disable=too-many-arguments,too-many-positional-arguments
        """Format an output message about a task/result."""
        return base.format(recipe_id=recipe_id, task_id=task_id,
                           statusresult=statusresult, sign=sign, testname=testname)

    def eval(self, **kwargs):
        """Evaluate whether set conditions are fulfilled."""
        if not self.kwargs:
            # Don't match empty conditions as satisfied.
            return None

        for key in self.kwargs:
            # We're supposed to match a key that isn't specified in the
            # condition, so we don't know what the result is.
            if key != 'status' and key not in kwargs:
                LOGGER.error('offending key: %s', key)
                raise RuntimeError('invalid arguments')

        for arg, requested_condition in self.kwargs.items():
            if kwargs[arg] not in requested_condition:
                return None

        # the status entry matches all the conditions specified
        return True
