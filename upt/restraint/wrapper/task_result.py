"""A result of a task run, with extra arguments to simplify working with it."""
import pathlib
import typing

from cki_lib import misc as cki_misc
from cki_lib.kcidb import sanitize_kcidb_status

from upt.logger import LOGGER
from upt.plumbing.objects import Host
from upt.restraint.file import RestraintResult
from upt.restraint.file import RestraintTask

from . import misc
from .dataclasses import ConsoleTask
from .dataclasses import TaskExecutionEnvironment


class SubTaskResult:
    # pylint: disable=too-few-public-methods
    """Definition of a subtask result."""

    def __init__(self, restraint_result: RestraintResult,
                 task_output_path: pathlib.Path) -> None:
        """Init."""
        # Ignoring mypy checks, because in the RestraintResult all attributes
        # are optionals, but attributes always are present in the restraint file.
        self.result_id: int = int(restraint_result.id)  # type: ignore
        self.name: str = restraint_result.path   # type: ignore
        self.output_path: pathlib.Path = pathlib.Path(task_output_path,
                                                      'results',
                                                      f'{self.result_id}',
                                                      'logs'
                                                      )
        # In KCIDB the keyword is `status` but in restraint and beaker the keyword is `result`
        # We need to sanitize it.
        self.status: str = sanitize_kcidb_status(str(restraint_result.result))

    @property
    def output_files(self) -> typing.Iterable[pathlib.Path]:
        """
        Return a list of files for the result.

        We can get the information from the restraint task or looking into the directory.

        We're not getting logs from the restraint file because time sometimes does not
        provide all logs. We have at least two identified cases:

        * When multihost, it can update log files later, even when they existed previously.
        * Sometimes misses some log files, and those files exist in the filesystem.
        """
        return [f for f in self.output_path.rglob("*") if f.is_file()]


class TaskResult:
    # pylint: disable=too-many-instance-attributes
    """A result of task that was run."""

    def __init__(
        self,
        host: Host,
        restraint_task: RestraintTask,
        console_task: ConsoleTask,
        environment: typing.Optional[TaskExecutionEnvironment] = None,
    ):
        """
        Create the object.

        To generate a task result we need different sources:

        * Host: (could be deprecated) information about the host where the task was executed.
        * RestraintTask: Information coming from the XML restraint file, this information
          could be updated, or we can only get the original information sent to restraint.
        * ConsoleTask: Information from the task got from the restraint console output
        * TaskExecutionEnvironment: Environment information related to the task
        """
        self.host: Host = host
        self.restraint_task: RestraintTask = restraint_task
        self.console_task: ConsoleTask = console_task
        self.environment: typing.Optional[TaskExecutionEnvironment] = environment

        # Derived attributes
        self.recipe_id: int = self._get_recipe_id()
        self.task_id: int = self._get_task_id()
        self.output_path: typing.Optional[pathlib.Path] = self._get_output_path()
        self.start_time: str = self._get_start_time()
        # Calculate result and get status from the parameter.
        self.status: typing.Optional[str] = self.console_task.status
        self.result: str = self._get_task_result()

        # Aggregated result, called "status" in kcidb. Set by protocol runner on each action.
        self.kcidb_status: typing.Optional[str] = None
        self.statusresult = self.console_task.status if not self.console_task.result \
            else f'{self.console_task.status}: {self.console_task.result}'

        self.waived: bool = self.restraint_task.is_waived
        self.testname: str = self._get_task_name() or ''

        self.fetch_url: typing.Optional[str] = \
            self.restraint_task.fetch.url if self.restraint_task.fetch else None

        self.test_maintainers = []
        # Comes from kpet-db.
        for maint in self.restraint_task.maintainers:
            self.test_maintainers.append(
                {
                    'name': maint.name,
                    'email': maint.email,
                    'gitlab': maint.gitlab
                })

        # Comes from kpet-db
        self.universal_id: typing.Optional[str] \
            = restraint_task.get_param_value_by_name('CKI_UNIVERSAL_ID')
        self.cki_name: typing.Optional[str] = restraint_task.get_param_value_by_name('CKI_NAME')
        self.cki_id: typing.Optional[str] = restraint_task.get_param_value_by_name('CKI_ID')
        self.cki_polarion_id: typing.Optional[str] \
            = restraint_task.get_param_value_by_name('CKI_POLARION_ID')

        self.lwd_hit: bool = restraint_task.hit_localwatchdog
        self.ewd_hit: bool = self.environment.is_ewd_hit if self.environment else False

        self.is_multihost = self._get_multihost()

        self.results: typing.List[SubTaskResult] = self._get_results()

        # Add debug if we don't have any subtest results when Console Task is 'Completed' or
        # 'Aborted'
        if self.console_task.status in ('Completed', 'Aborted') and not self.restraint_task.results:
            LOGGER.info('The task %s does not have any subtest', self.testname)

    def _get_recipe_id(self) -> int:
        """
        Get recipe id.

        This information could come from:
        * Host
        * RestraintTask (if the restraint file is updated).

        Currently, we're getting this value from Host
        """
        return int(self.host.recipe_id)

    def _get_multihost(self) -> bool:
        """Check if the task is running in a multihost environment."""
        multihost = False

        if self.environment:
            multihost = self.environment.is_multihost

        return multihost

    def _get_output_path(self) -> typing.Optional[pathlib.Path]:
        """Get the output path for files, dumps, logs, etc."""
        if self.environment:
            return pathlib.Path(
                self.environment.restraint_output_path,
                'recipes',
                f'{self.recipe_id}',
                'tasks',
                f'{self.task_id}'
            )
        return None

    def _get_task_id(self) -> int:
        """
        Get task id.

        This information could come from:
        * RestraintTask (if the restraint file is updated).
        * ConsoleTask

        Currently, we're getting this value from ConsoleTask
        """
        return self.console_task.task_id

    def _get_task_name(self) -> typing.Optional[str]:
        """
        Get task name.

        This information could come from:
        * RestraintTask
        * ConsoleTask

        Currently, we're getting this value from RestraintTask
        """
        return self.restraint_task.name

    def _get_start_time(self) -> str:
        """
        Get start_time.

        This information could come from restraint_task, otherwise
        we will set with the actual time.

        If the information comes from the restraint task, we should transform it.

        Example from restraint_task
          * Original: 2022-05-06T20:26:58+1230
          * Returned: 2022-05-06T20:26:58+12:30
        """
        if self.restraint_task.start_time:
            left_part = self.restraint_task.start_time[:-2]
            right_part = self.restraint_task.start_time[-2:]
            return f'{left_part}:{right_part}'
        return cki_misc.utc_now_iso()  # type: ignore

    @property
    def is_cki_test(self) -> bool:
        """Check if a test should be considered as a CKI TEST."""
        return self.restraint_task.cki_is_test

    @property
    def is_boot_task(self) -> bool:
        """Check if the task is a boot task."""
        return self.universal_id == 'boot' or self.testname.lower() == 'boot test'

    @property
    def output_files(self) -> typing.Iterable[pathlib.Path]:
        """
        Return a list of files for the task.

        We can get the information from the restraint task or looking into the directory.

        We're not getting logs from the restraint file because time sometimes does not
        provide all logs. We have at least two identified cases:

        * When multihost, it can update log files later, even when they existed previously.
        * Sometimes misses some log files, and those files exist in the filesystem.

        If we don't have restraint results we'll get all output files, otherwise
        we only will get the output files for the common section.
        """
        if self.output_path:
            if self.results:
                task_output_path = pathlib.Path(self.output_path, 'logs')
            else:
                task_output_path = self.output_path

            return [f for f in task_output_path.rglob("*") if f.is_file()]

        return []

    def _get_task_result(self) -> str:
        """Calculate task result."""
        if self.status == 'Completed' and not self.console_task.result \
                and self.restraint_task.has_skipped_results:
            return 'SKIP'
        return self.console_task.result

    def _get_results(self) -> typing.List[SubTaskResult]:
        """
        Get a list of valid results.

        A valid result is a result with logs, so need to handle particular cases,
        without logs:

        * If 99_reboot and 10_localwatchdog are present, we're going to skip 99_reboot.

        * If the result is produced by the restrain local watchdog, in that
          case we're going to generate a fake one, adding a fake file.

        * If the result is produced by an exit_code, in that case we're going to
          generate a fake log file, adding to the result.

        * If the result does not belong to any of the previous cases, we're going to
          generate a fake log file, adding to the result.

        Issues related:
        * https://gitlab.com/cki-project/upt/-/issues/125
        * https://gitlab.com/cki-project/upt/-/issues/133
        * https://gitlab.com/cki-project/upt/-/issues/141
        * https://gitlab.com/cki-project/upt/-/issues/170
        * https://gitlab.com/cki-project/upt/-/issues/171
        """
        results = []
        if self.output_path:
            has_localwatchdog = any(
                result.path.endswith('10_localwatchdog')  # type: ignore[union-attr]
                for result in self.restraint_task.results
            )
            for result in self.restraint_task.results:
                if has_localwatchdog and \
                   result.path.endswith('99_reboot'):  # type: ignore[union-attr]
                    continue
                if result.path.endswith('10_localwatchdog'):  # type: ignore[union-attr]
                    file = pathlib.Path(
                        self.output_path,
                        'results', result.id, 'logs', 'test_timeout.log')  # type: ignore[arg-type]
                    content = 'Test aborted due to a timeout'
                    result.logs.append(
                        misc.create_restraint_log(file, content, self.output_path)
                    )
                if result.path.endswith('exit_code'):  # type: ignore[union-attr]
                    file = pathlib.Path(
                        self.output_path,
                        'results', result.id, 'logs', 'exit_code.log')  # type: ignore[arg-type]
                    result.logs.append(
                        misc.create_restraint_log(file, result.text, self.output_path)
                    )
                if not result.logs:
                    file = pathlib.Path(
                        self.output_path,
                        'results', result.id, 'logs', 'no_logs.log')  # type: ignore[arg-type]
                    content = 'The result does not contain logs, please look at taskout.log'
                    result.logs.append(
                        misc.create_restraint_log(file, content, self.output_path)
                    )

                results.append(SubTaskResult(result, self.output_path))

        return results

    def add_panic_subtest(self) -> None:
        """
        Add a panic subtest result.

        Restraint in standalone mode does not provide any information about kernel panics.

        We need to create a new one and add it to the results.

        In this scenario, we don't have any Restraint XML file, we need to create it from scratch.

        If self.output_path does not have any value, restraint execution is broken and we can't
        create the result.
        """
        if self.output_path:
            result = RestraintResult.create_from_scratch()
            result.id = '1000'
            result.path = 'Panic'
            result.result = 'FAIL'
            file = pathlib.Path(
                self.output_path,
                'results', result.id, 'logs', 'test_kernel_panic.log'
            )
            content = 'Test aborted due to a kernel panic, please review the console.log file.'

            result.logs.append(
                misc.create_restraint_log(file, content, self.output_path)
            )

            self.results.append(SubTaskResult(result, self.output_path))
            LOGGER.info('Panic subtest added')
        else:
            LOGGER.info('Unable to add the Panic subtest')
