"""Misc utility methods."""
import os
import pathlib
import typing
from urllib.parse import quote
from urllib.parse import unquote

from twisted.internet import error

from upt.restraint.file import RestraintLog


def add_directory_suffix(output, instance_no):
    """Add numerical .done.XY suffix to directory."""
    dirname = f'{output}.done.{instance_no:02d}'
    assert not os.path.isdir(dirname)
    return dirname


def attempt_reactor_stop(reactor):
    """If twisted reactor is running, try to stop it."""
    if reactor.running:
        try:
            reactor.callFromThread(reactor.stop)
        except error.ReactorNotRunning:
            pass


def attempt_heartbeat_stop(heartbeat_loop):
    """Attempt to stop heartbeat looping call."""
    try:
        heartbeat_loop.stop()
    except (AssertionError,  AttributeError):
        pass


def convert_path_to_link(path: typing.Union[str, pathlib.Path], is_file: bool = True) -> str:
    """Get a quoted url to path, or just path if we're not running in pipeline."""
    if prefix := os.environ.get(f'UPT_ARTIFACT_{"FILE" if is_file else "DIRECTORY"}_URL_PREFIX'):
        return prefix + quote(str(path))
    return str(path)


def fix_file_name_encoded_as_html(path: pathlib.Path) -> pathlib.Path:
    """
    Clean file name encoded as HTML.

    When running restraint in standalone mode file names are HTML encoded.

    If we run restraint with beaker, file names are not HTML encoded.

    Example:
      * restraint standalone produces 'RFC%203961.log'
      * restraint with beaker produces 'RFC 3961.log'
    """
    file_name = path.name
    new_name = unquote(file_name, encoding='utf-8')
    if file_name != new_name:
        directory = path.parent
        return path.rename(pathlib.Path(directory, new_name))

    return path


def create_restraint_log(file: pathlib.Path,
                         content: str,
                         relative_path: pathlib.Path) -> RestraintLog:
    """
    Create a Restraint Log.

    With the path and content we can create the file, but we need to relative path
    because path in RestraintLog are relative.

    If it needed this function will make all needed directories.
    """
    file.parent.mkdir(parents=True, exist_ok=True)
    file.write_text(content, encoding='utf-8')
    log = RestraintLog.create_from_scratch()
    log.path = str(file.relative_to(relative_path))
    log.filename = file.name
    return log
