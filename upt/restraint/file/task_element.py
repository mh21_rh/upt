"""Restraint Task Element."""

from __future__ import annotations

import copy
import re
import typing
import xml.etree.ElementTree as ET

from cki_lib import misc

from upt.restraint.wrapper.const import RGX_MAINTAINERS

from .common_element import CommonXMLElement
from .restraint_dataclasses import RestraintTaskDiff
from .result_element import RestraintResult
from .simple_elements import RestraintFetch
from .simple_elements import RestraintLog
from .simple_elements import RestraintParam
from .utils import generate_empty_element


class RestraintTask(CommonXMLElement):
    """
    Task Class.

    A task class should contain a list of logs, a fetch element, a list
    of parameters and a list of results.

    Inside of the parameters section, we have some CKI information, and the
    maintainers list is in the CKI_MAINTAINERS parameters, and we want to manage
    as a separate class.

    You can see an example in examples.py file (task_example).
    """

    _attributes = ['duration', 'end_time', 'id', 'name', 'result',
                   'role', 'start_time', 'status']
    _name = 'task'
    _delegated_items = ['logs', 'fetch', 'params', 'results']

    def __init__(self, element: ET.Element):
        """Initialize."""
        super().__init__(element)
        self.logs = self._process_logs()
        self.fetch = self._process_fetch()
        self.params = self._process_params()
        self.maintainers = self._process_maintainers()
        self.results = self._process_results()
        self._clear()

    @property
    def cki_is_test(self) -> bool:
        """Check if the task is a cki test."""
        # Before to 2023-02-20, tasks don't have CKI_IS_TEST, we need to
        # keep the backward compatibility for a while
        is_cki_test = self.get_param_value_by_name('CKI_IS_TEST')
        if is_cki_test is not None:
            return misc.strtobool(is_cki_test)
        return self.get_param_value_by_name('CKI_UNIVERSAL_ID') is not None and \
            self.get_param_value_by_name('CKI_NAME') is not None

    @property
    def is_boot_task(self) -> bool:
        """Check if the task is a boot task."""
        if self.name is not None:
            is_name_boot_test = self.name.lower() == 'boot test'
        else:
            is_name_boot_test = False
        return self.get_param_value_by_name('CKI_UNIVERSAL_ID') == 'boot' or \
            is_name_boot_test

    @property
    def hit_localwatchdog(self) -> bool:
        """Check if the task hit the localwatchdog."""
        return any(result.path == '/10_localwatchdog' for result in self.results)

    @property
    def cki_id(self) -> typing.Optional[str]:
        """Return the CKI_ID."""
        return self.get_param_value_by_name("CKI_ID")

    @property
    def is_waived(self) -> bool:
        """Check if a task can be waived."""
        waived = False
        if (is_waived := self.get_param_value_by_name('CKI_WAIVED')):
            waived = misc.strtobool(is_waived)
        return waived

    @property
    def has_skipped_results(self) -> bool:
        """Check if the task contains skipped results."""
        return any(result.result == 'SKIP' for result in self.results)

    def _process_params(self) -> typing.List[RestraintParam]:
        """Process parameters."""
        params: typing.List[RestraintParam] = []
        if (xml_params := self.element.find('.//params')) is not None:
            for param in xml_params.findall('param'):
                params.append(RestraintParam(param))
        return params

    def _process_fetch(self) -> typing.Optional[RestraintFetch]:
        """Process fetch."""
        fetch: typing.Optional[RestraintFetch] = None
        if (xml_fetch := self.element.find('.//fetch')) is not None:
            fetch = RestraintFetch(xml_fetch)
        return fetch

    def _process_logs(self) -> typing.List[RestraintLog]:
        """Process logs."""
        logs: typing.List[RestraintLog] = []
        if (xml_logs := self.element.find('.//logs')) is not None:
            for log in xml_logs.findall('log'):
                logs.append(RestraintLog(log))
        return logs

    def _process_maintainers(self) -> typing.List[TestMaintainer]:
        """Process maintainers."""
        maintainers: typing.List[TestMaintainer] = []
        if maintainers_raw_value := self.get_param_value_by_name('CKI_MAINTAINERS'):
            # Create the maintainers list:
            for name, email, gitlab in re.findall(RGX_MAINTAINERS, maintainers_raw_value):
                maintainers.append(TestMaintainer(name, email, gitlab))
            # Remove the list from parameters list
            for param in self.params:
                if param.name == 'CKI_MAINTAINERS':
                    self.params.remove(param)
                    break
        return maintainers

    def _process_results(self) -> typing.List[RestraintResult]:
        """Process all the results of the class."""
        results: typing.List[RestraintResult] = []
        if (xml_results := self.element.find('.//results')) is not None:
            for result in xml_results.findall('result'):
                results.append(RestraintResult(result))
        return results

    def get_param_value_by_name(self, name: str) -> typing.Optional[str]:
        """Get param value by name."""
        for param in self.params:
            if param.name == name:
                return param.value
        return None

    def create_or_update_maintainer(
        self,
        name: str,
        email: str,
        gitlab: typing.Optional[str] = None,
    ) -> None:
        """Create or update a maintainer."""
        exists = False
        for maintainer in self.maintainers:
            if maintainer.email == email:
                maintainer.name = name
                maintainer.gitlab = gitlab
                exists = True
                break
        if not exists:
            self.maintainers.append(TestMaintainer(name, email, gitlab))

    def create_or_update_log(self, path: str, filename: str) -> None:
        """Create or update a log."""
        exists = False
        for log in self.logs:
            if log.path == path:
                log.filename = filename
                exists = True
                break
        if not exists:
            restraint_log = RestraintLog.create_from_scratch()
            restraint_log.path = path
            restraint_log.filename = filename
            self.logs.append(restraint_log)

    def create_or_update_paramater(self, name: str, value: str) -> None:
        """Create or update parameter."""
        exists = False
        for param in self.params:
            if param.name == name:
                param.value = value
                exists = True
                break
        if not exists:
            param = RestraintParam.create_from_scratch()
            param.name = name
            param.value = value
            self.params.append(param)

    def generate_xml_object(self) -> ET.Element:
        """Generate the object."""
        # clean the element
        self._clear()
        # Generate and add logs
        if self.logs:
            logs = generate_empty_element('logs')
            for log in self.logs:
                logs.append(log.generate_xml_object())
            self.element.append(logs)
        # Generatare and add fetch
        if self.fetch:
            self.element.append(self.fetch.generate_xml_object())
        # Generate parameters
        params = generate_empty_element('params')
        for param in self.params:
            params.append(param.generate_xml_object())
        # Generate parameters
        if self.maintainers:
            param = RestraintParam.create_from_scratch()
            param.name = 'CKI_MAINTAINERS'
            param.value = ', '.join(map(str, self.maintainers))
            params.append(param.generate_xml_object())
        # Add parameters only if we have any parameter
        if len(params.findall('.//param')):
            self.element.append(params)
        # Generate and add results
        if self.results:
            results = generate_empty_element('results')
            for result in self.results:
                results.append(result.generate_xml_object())
            self.element.append(results)
        return copy.copy(self.element)

    def get_result_by_id(self, result_id: str) -> typing.Optional[RestraintResult]:
        """Get RestraintResult by id.

        Args:
            result_id - str, the id that uniquely identifies the RestraintResult.
        """
        return next((result for result in self.results
                     if result.id == result_id), None)

    def diff(self, other: RestraintTask) -> typing.Optional[RestraintTaskDiff]:
        """Generate a diff between two tasks."""
        if not isinstance(other, RestraintTask):
            raise TypeError('Both objects must be RestraintTask')
        if self.id is None or other.id is None:
            raise ValueError('id must be defined in both RestraintTasks')
        if self.id != other.id:
            raise ValueError('Tasks do not have the same id')

        if changes := self.get_diff_attributes(other):
            return RestraintTaskDiff(self.id, self.cki_id, changes)

        return None


class TestMaintainer:
    """Test Maintainer.

    This class represents a Test Maintainer, all maintainers are exposed as a
    parameter in the Task XML element.
    """

    gitlab: typing.Optional[str]

    # disable pytint warning
    __test__ = False

    email_regex = r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b'

    def __init__(self, name: str, email: str, gitlab: typing.Optional[str] = None):
        """Initialize."""
        self.name = name
        self.email = email
        self.gitlab = gitlab

    @property
    def email(self) -> str:
        """Get email."""
        return self.__email

    @email.setter
    def email(self, value: str) -> None:
        """Set email."""
        if re.fullmatch(self.email_regex, value):
            self.__email = value
        else:
            raise ValueError(f'Invalid email address: {value}')

    def __str__(self) -> str:
        """Get CKI_MAINTAINERS format required."""
        if self.gitlab:
            return f'{self.name} <{self.email}> / {self.gitlab}'
        return f'{self.name} <{self.email}>'
