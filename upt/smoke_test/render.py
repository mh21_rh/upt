"""Render Templates."""

import argparse
from importlib.resources import files
import sys
import typing

import jinja2

ARCHITECTURES: typing.List[str] = ['aarch64', 's390x', 'ppc64le', 'x86_64']
DISTROS: typing.List[str] = [
    'CentOSStream9',
    'Fedora35',
    'Fedora36',
    'Fedora37',
    'FedoraELN',
    'Fedorarawhide',
    'RedHatEnterpriseLinux7',
    'RedHatEnterpriseLinux8',
    'RedHatEnterpriseLinux9',
]
TEMPLATES_DIR = str(files(__package__).joinpath('templates'))

# smoke tests
MULTIHOSTS_SMOKE_TESTS: typing.List[str] = [
    'multihosts_abort_recipe', 'multihosts_abort_fail', 'multihosts_panic',
    'multihosts_skip', 'multihosts_fail', 'multihosts_warn'
]
SINGLE_SMOKE_TESTS: typing.List[str] = [
    'abort_recipe', 'abort_task', 'fail',
    'kernel_panic', 'skip', 'timeout', 'pass', 'warn'
]
SMOKE_TESTS_GROUPS: typing.Dict[str, typing.List[str]] = {
    'all': SINGLE_SMOKE_TESTS + MULTIHOSTS_SMOKE_TESTS,
    'single': SINGLE_SMOKE_TESTS,
    'multihosts': MULTIHOSTS_SMOKE_TESTS
}


def get_smoke_tests(original_smoke_tests: typing.Optional[typing.List[str]]) -> typing.List[str]:
    """Generate and flat smoke tests."""
    # Use default one if not option is defined
    if not original_smoke_tests:
        return sorted(SINGLE_SMOKE_TESTS)

    smoke_tests = set(original_smoke_tests)
    # Expand lists when using groups (all, single, multihosts)
    for group_name, grouped_smoke_tests in SMOKE_TESTS_GROUPS.items():
        if group_name in smoke_tests:
            smoke_tests.remove(group_name)
            smoke_tests.update(grouped_smoke_tests)

    # Sort and return
    return sorted(smoke_tests)


def get_template_variables(
    parsed_vars: typing.Dict[str, typing.Any],
) -> typing.Dict[str, typing.Any]:
    """Clean and return variables to generate templates."""
    return {
        'arch': parsed_vars['arch'],
        'distro': parsed_vars['distro'],
        'smoke_tests': get_smoke_tests(parsed_vars['smoke_tests'])
    }


def render(template_file: str, data: typing.Dict[str, typing.Any]) -> str:
    """Render Beaker Template."""
    env = jinja2.Environment(
        loader=jinja2.FileSystemLoader(TEMPLATES_DIR),
        trim_blocks=True,
        keep_trailing_newline=True,
        lstrip_blocks=True,
        autoescape=jinja2.select_autoescape(
            enabled_extensions=('xml'),
            default_for_string=True,
        ),
        undefined=jinja2.StrictUndefined,
    )

    return env.get_template(template_file).render(data)


def main(args: typing.List[str]) -> None:
    """Run the main CLI interface."""
    smoke_test_choices = SINGLE_SMOKE_TESTS + MULTIHOSTS_SMOKE_TESTS
    for group in SMOKE_TESTS_GROUPS:
        smoke_test_choices.append(group)
    parser = argparse.ArgumentParser(
        description='Render Beaker XML for upt smoke tests',
    )

    parser.add_argument('--smoke-test', type=str, action='append', dest='smoke_tests',
                        choices=smoke_test_choices,
                        help=f'Select smoke test. Defaults single smoke tests {SINGLE_SMOKE_TESTS}')

    parser.add_argument('--arch', type=str, default='x86_64', choices=ARCHITECTURES,
                        help='Select the architecture. Defaults to x86_64')

    parser.add_argument('--distro', type=str, default='CentOSStream9', choices=DISTROS,
                        help='Select the distro. Defaults to CentOSStream9')

    parser.add_argument('--output', type=argparse.FileType('w'), default=sys.stdout,
                        help='Output path for the rendered file. Defaults STDOUT')

    parsed_args = parser.parse_args(args)

    template_file = 'smoke_test.j2'
    template_variables = get_template_variables(vars(parsed_args))

    result = render(template_file, template_variables)
    parsed_args.output.write(result)


if __name__ == '__main__':
    main(sys.argv[1:])
