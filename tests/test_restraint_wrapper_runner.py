"""Test cases for upt.restraint.wrapper/runner module."""
import itertools
import json
import os
import pathlib
import unittest
from unittest import mock

from cki_lib.kcidb.file import KCIDBFile
from cki_lib.kcidb.validate import PRODUCER_KCIDB_SCHEMA
from twisted.internet.error import ReactorNotRunning

from tests.const import ASSETS_DIR
from tests.utils import create_temporary_kcidb
from upt.plumbing.format import ProvisionData
from upt.plumbing.objects import Host
from upt.restraint.wrapper.protocol import RestraintClientProcessProtocol
from upt.restraint.wrapper.runner import Runner

# pylint: disable=no-self-use,too-many-public-methods


class TestRunner(unittest.TestCase):
    """Test cases for runner module."""

    def tearDown(self) -> None:
        self.mock_prov.stop()

    @mock.patch('upt.restraint.wrapper.protocol.Watcher', mock.Mock())
    def setUp(self) -> None:
        with create_temporary_kcidb() as kcidb_file:
            kcidb_data = KCIDBFile(kcidb_file)
        self.std_kwargs = {'keycheck': 'no', 'reruns': 3, 'output': '/tmp/a',
                           'input': None, 'dump': True,
                           'kcidb_data': kcidb_data}
        self.req_asset = os.path.join(ASSETS_DIR, 'req.yaml')

        self.provision_data = ProvisionData.deserialize_file(self.req_asset)
        self.beaker = self.provision_data.get_provisioner('beaker')
        for host in self.beaker.rgs[0].recipeset.hosts:
            host.counter = mock.Mock()

        self.mock_prov = mock.patch('upt.restraint.wrapper.runner.ProvisionData.deserialize_file',
                                    lambda *x: self.provision_data)
        self.mock_prov.start()
        mock1 = mock.patch('upt.restraint.wrapper.testplan.LOGGER.printc', mock.Mock())
        mock1.start()

    @mock.patch('upt.restraint.wrapper.runner.Runner.add_protocol')
    @mock.patch('upt.restraint.wrapper.testplan.KCIDBTestAdapter', mock.Mock())
    @mock.patch('builtins.print', mock.Mock())
    def test_init(self, mock_add_protocol):
        """Ensure runner creates protocols."""
        Runner([self.beaker], **self.std_kwargs)
        mock_add_protocol.assert_called()

    @mock.patch('upt.restraint.wrapper.runner.Runner.add_protocol', mock.Mock())
    @mock.patch('upt.restraint.wrapper.testplan.KCIDBTestAdapter', mock.Mock())
    @mock.patch('upt.logger.LOGGER.debug')
    def test_start_protocols(self, mock_debug):
        """Ensure start_protocols works."""
        runner = Runner([self.beaker], **self.std_kwargs)
        mock1 = mock.MagicMock()
        mock1.proc = None

        mock2 = mock.MagicMock()

        mock3 = mock.MagicMock()
        mock3.proc = 'process-created'
        mock3.run_suffix = ''

        with mock.patch.object(runner, 'protocols', []):
            runner.start_protocols()
            mock_debug.assert_not_called()

        with mock.patch.object(runner, 'protocols', [mock1]):
            runner.start_protocols()
            mock_debug.assert_called_with(
                'Starting protocol for resource_id %s...', mock1.resource_group.resource_id)
            mock1.start_all.assert_called()

        with mock.patch.object(runner, 'protocols', [mock2]):
            runner.start_protocols()
            mock2.start_all.assert_not_called()

        with mock.patch.object(runner, 'protocols', [mock3]):
            runner.start_protocols()
            mock3.start_all.assert_not_called()

    @mock.patch('upt.restraint.wrapper.testplan.KCIDBTestAdapter', mock.Mock())
    @mock.patch('upt.restraint.wrapper.runner.Runner.add_protocol', mock.Mock())
    @mock.patch('upt.restraint.wrapper.runner.reactor')
    @mock.patch('upt.restraint.wrapper.runner.Runner.start_protocols')
    def test_wait_on_protocols(self, mock_start_protocols, mock_reactor):
        """Ensure wait_on_protocols works."""
        runner = Runner([self.beaker], **self.std_kwargs)

        mock_reactor.running = True
        with mock.patch('upt.plumbing.interface.ProvisionerCore.all_recipes_finished',
                        lambda *x: False):
            runner.wait_on_protocols()
            mock_reactor.callFromThread.assert_not_called()
            assert mock_start_protocols.call_count == 1

        with mock.patch('upt.plumbing.interface.ProvisionerCore.all_recipes_finished',
                        lambda *x: True):
            with mock.patch.object(runner, 'protocols', [None]):
                mock_reactor.running = False
                runner.wait_on_protocols()
                mock_reactor.callFromThread.assert_not_called()
                assert mock_start_protocols.call_count == 2

                mock_reactor.running = True
                runner.wait_on_protocols()
                assert mock_start_protocols.call_count == 3
                mock_reactor.callFromThread.assert_called()

                mock_reactor.callFromThread.side_effect = itertools.chain([ReactorNotRunning()],
                                                                          itertools.cycle([None]))
                runner.wait_on_protocols()

    @mock.patch.dict(os.environ, {'KCIDB_CHECKOUT_ID': 'redhat:5678'})
    @mock.patch('upt.restraint.wrapper.runner.Runner.add_protocol', mock.Mock())
    @mock.patch('upt.restraint.wrapper.testplan.KCIDBTestAdapter', mock.Mock())
    @mock.patch('upt.restraint.wrapper.runner.TestPlan', mock.Mock())
    @mock.patch('upt.restraint.wrapper.runner.reactor')
    @mock.patch('upt.restraint.wrapper.runner.task')
    @mock.patch('upt.restraint.wrapper.runner.Runner.download_logs')
    @mock.patch('upt.logger.LOGGER.debug')
    def test_run(self, mock_debug, mock_download, mock_task, mock_reactor):
        """Ensure run works."""
        runner = Runner([self.beaker], **self.std_kwargs)

        runner.run()

        mock_reactor.addSystemEventTrigger.assert_called_with(
            'before', 'shutdown', runner.cleanup_handler)
        mock_debug.assert_called_with('Runner waiting for processes to finish...')
        mock_download.assert_called()

        mock_task.LoopingCall.assert_called_with(runner.wait_on_protocols)

    @mock.patch.dict(os.environ, {'KCIDB_CHECKOUT_ID': 'redhat:5678'})
    @mock.patch('upt.restraint.wrapper.runner.Runner.add_protocol', mock.Mock())
    @mock.patch('upt.restraint.wrapper.runner.Runner.download_logs')
    @mock.patch('upt.restraint.wrapper.runner.reactor', mock.Mock())
    @mock.patch('upt.restraint.wrapper.runner.task', mock.Mock())
    @mock.patch('upt.restraint.wrapper.testplan.KCIDBTestAdapter', mock.Mock())
    @mock.patch('builtins.print', mock.Mock())
    def test_run_adapter(self, mock_download):
        """Ensure run calls testplan dump method when adapter is used."""
        mock_download.return_value = []
        new_args = self.std_kwargs.copy()
        new_args['dump'] = False
        runner = Runner([self.beaker], **new_args)
        with mock.patch.object(runner.testplan, 'dump_testplan') as mock_testplan:
            runner.run()
            mock_testplan.assert_not_called()

        runner = Runner([self.beaker], **self.std_kwargs)
        with mock.patch.object(runner.testplan, 'dump_testplan') as mock_testplan:
            runner.run()
            mock_testplan.assert_called()

    @mock.patch('upt.restraint.wrapper.runner.Runner.add_protocol', mock.Mock())
    @mock.patch('upt.restraint.wrapper.testplan.KCIDBTestAdapter', mock.Mock())
    @mock.patch('upt.logger.LOGGER.error')
    def test_handle_runner_failure(self, mock_error):
        """Ensure handle_runner_failure works."""
        runner = Runner([self.beaker], **self.std_kwargs)
        fail_mock = mock.Mock()
        runner.handle_runner_failure(fail_mock)

        mock_error.assert_called_with('FAILURE in deferred: %s', fail_mock)

    @mock.patch('upt.restraint.wrapper.runner.Runner.add_protocol', mock.Mock())
    @mock.patch('upt.restraint.wrapper.testplan.KCIDBTestAdapter', mock.Mock())
    @mock.patch('upt.logger.LOGGER.debug')
    @mock.patch('builtins.print')
    def test_cleanup_handler(self, mock_print, mock_debug):
        """Ensure cleanup_handler works."""
        runner = Runner([self.beaker], **self.std_kwargs)

        mock0 = mock.MagicMock()
        mock1 = mock.MagicMock()
        mock2 = mock.MagicMock()
        with mock.patch.object(runner, 'protocols', [mock0, mock1, mock2]):
            with mock.patch.object(self.beaker, 'release_resources'):
                mock0.proc = None

                mock1.proc.status = 'whatever'
                mock1.proc._getReason.return_value = 'ended'

                mock2.proc.status = 'whatever'
                mock2.proc._getReason.return_value = 'ended by signal 2'

                runner.cleanup_handler()

                mock_print.assert_any_call('* Abnormal exit!')
                mock_debug.assert_any_call('* Runner cleanup handler runs...')
                self.assertTrue(runner.cleanup_done)

    @mock.patch('upt.restraint.wrapper.runner.Runner.add_protocol', mock.Mock())
    @mock.patch('builtins.print')
    def test_cleanup_handler_pass(self, mock_print):
        """Ensure cleanup_handler works."""
        self.std_kwargs['dump'] = False
        runner = Runner([self.beaker], **self.std_kwargs)

        mock0 = mock.MagicMock()
        mock0.kernel_failed_testing = False
        with mock.patch.object(runner.provisioners[0].rgs[0].recipeset.hosts[1],
                               'done_processing', True):
            with mock.patch.object(runner, 'protocols', [mock0]):
                runner.cleanup_handler()
                mock_print.assert_any_call('* Abnormal exit!')

    @mock.patch('upt.restraint.wrapper.runner.Runner.add_protocol', mock.Mock())
    @mock.patch('builtins.print')
    def test_cleanup_handler_normal_exit(self, mock_print):
        """Ensure cleanup_handler works and that 'abnormal exit' warning isn't printed."""
        self.std_kwargs['dump'] = False
        runner = Runner([self.beaker], **self.std_kwargs)

        mock0 = mock.MagicMock()
        mock0.kernel_failed_testing = False

        runner = Runner([self.beaker], **self.std_kwargs)

        host = Host()
        host.done_processing = True
        with mock.patch.object(runner.provisioners[0].rgs[0].recipeset, 'hosts', [host]):
            with mock.patch.object(runner, 'protocols', [mock0]):
                runner.cleanup_handler()

    @mock.patch('upt.restraint.wrapper.runner.Runner.add_protocol', mock.Mock())
    @mock.patch('upt.restraint.wrapper.testplan.KCIDBTestAdapter', mock.Mock())
    @mock.patch('upt.logger.LOGGER.debug')
    def test_cleanup_handler_done(self, mock_debug):
        """Ensure cleanup_handler works."""
        runner = Runner([self.beaker], **self.std_kwargs)
        runner.cleanup_done = True

        runner.cleanup_handler()

        mock_debug.assert_not_called()

    @mock.patch('upt.restraint.wrapper.runner.TestPlan.on_task_result')
    @mock.patch('upt.restraint.wrapper.testplan.KCIDBTestAdapter', mock.Mock())
    @mock.patch('upt.restraint.wrapper.runner.Runner.add_protocol', mock.Mock())
    def test_on_task_result_pass_to_testplan(self, mock_on_task_result_testplan):
        """Ensure on_task_result call is passed to testplan."""
        runner = Runner([self.beaker], **self.std_kwargs)
        runner.on_task_result(1, 2, 'w.json')

        mock_on_task_result_testplan.assert_called_with(1, 2, 'w.json')

    @mock.patch('upt.restraint.wrapper.runner.TestPlan.create_testplan', mock.Mock())
    @mock.patch('upt.restraint.wrapper.testplan.KCIDBTestAdapter', mock.Mock())
    @mock.patch('upt.restraint.wrapper.protocol.ShellWrap', mock.Mock())
    @mock.patch('upt.restraint.wrapper.protocol.RestraintHost', mock.Mock())
    @mock.patch('upt.restraint.wrapper.protocol.RestraintClientProcessProtocol._get_reruns',
                mock.Mock())
    @mock.patch('upt.restraint.wrapper.protocol.RestraintClientProcessProtocol._is_multihost',
                mock.Mock())
    @mock.patch('upt.provisioners.beaker.Beaker.set_reservation_duration', mock.Mock())
    @mock.patch('upt.provisioners.beaker.Beaker.set_reservation_duration', mock.Mock())
    def test_add_protocol(self):
        """Ensure add_protocol works."""
        with mock.patch('upt.restraint.wrapper.runner.Runner.add_protocol', mock.Mock()):
            runner = Runner([self.beaker], **self.std_kwargs)

        result = runner.add_protocol(self.beaker, self.beaker.rgs[0], None, **self.std_kwargs)
        self.assertIsInstance(result, RestraintClientProcessProtocol)

        runner.add_protocol(self.beaker, self.beaker.rgs[0], None, **self.std_kwargs)

    def test_part_fdir(self):
        """Ensure part_fdir works."""
        self.assertEqual(('J:1234', '123', '1'),
                         Runner.part_fdir('LOGS_J:1234_0_R_123_T_1_test-redhat_a1'))

    @mock.patch('upt.logger.LOGGER.debug')
    @mock.patch('upt.provisioners.beaker.Beaker.get_links2logs')
    @mock.patch('upt.provisioners.beaker.pathlib.Path')
    @mock.patch('upt.provisioners.beaker.sanitize_link')
    def test_download_logs(self, mock_sanitize, mock_path, mock_links, mock_debug):
        """Ensure download_logs works."""
        mock_links.return_value = ['url1', 'url2']

        Runner.download_logs([self.beaker, None], 'output', None)
        mock_sanitize.assert_any_call('url1', mock_path.return_value, None)
        mock_sanitize.assert_any_call('url2', mock_path.return_value, None)

        Runner.download_logs([self.beaker, None], 'output', '4.18.0-000.el8.test.dt2')
        mock_debug.assert_called_with('Downloading Beaker logs...')
        mock_sanitize.assert_any_call('url1', mock_path.return_value, '4.18.0-000.el8.test.dt2')
        mock_sanitize.assert_any_call('url2', mock_path.return_value, '4.18.0-000.el8.test.dt2')

    @mock.patch('upt.restraint.wrapper.runner.glob.glob')
    @mock.patch('upt.restraint.wrapper.runner.pathlib.Path')
    @mock.patch('cki_lib.kcidb.file.KCIDBFile.save')
    def test_update_kcidb_dumpfiles(self, mock_save, mock_path, mock_glob):
        """Ensure update_kcidb_dumpfiles works."""
        mock_glob.return_value = [pathlib.Path('test.json')]
        data = {
            'version': {'major': PRODUCER_KCIDB_SCHEMA.major, 'minor': PRODUCER_KCIDB_SCHEMA.minor},
            'checkouts': [],
            'builds': [],
            'tests': [{
                'build_id': 'redhat:1234',
                'id': 'redhat:3',
                'origin': 'redhat',
                'output_files': [{'name': 'f1', 'url': 'https://file1'}]
            }]
        }
        mock_path.return_value.read_text.return_value = json.dumps(data, indent=4)
        mock_path.return_value.is_file.return_value = True
        mock_path.return_value.name = 'test.json'

        Runner.update_kcidb_dumpfiles('output', [('/1', pathlib.Path('/path/to/logs/harness.log'))])
        mock_glob.assert_called_with('output/results_*/*.json')
        mock_save.assert_called()
