"""Test cases for upt.restraint.wrapper.shell module."""
import copy
import os
import unittest
import xml.etree.ElementTree as ET

from tests.const import ASSETS_DIR
from tests.utils import remove_spaces_and_new_lines
from upt import const
from upt.plumbing.format import ProvisionData
from upt.plumbing.objects import Host
from upt.restraint.file import RestraintRecipe
from upt.restraint.wrapper.shell import ShellWrap


class TestShellWrap(unittest.TestCase):
    """Test cases for restraint_shell module."""

    def setUp(self) -> None:
        self.req_asset = os.path.join(ASSETS_DIR, 'req.yaml')

        self.prov_data = ProvisionData.deserialize_file(self.req_asset)

    def test_wrap(self):
        """Ensure ShellWrap creates a directory."""
        resource_group = self.prov_data.get_provisioner('beaker').rgs[0]
        wrap = ShellWrap(resource_group, **{'keycheck': 'no'})
        temp_dir = wrap.tempdir
        self.assertTrue(os.path.isdir(temp_dir.name))

        del wrap

        self.assertFalse(os.path.isdir(temp_dir.name))

    def test_rerun_recipe_tasks_from_index(self):
        """Ensure that logic to build a shorter list of tasks (for rerun) works."""
        resource_group = copy.deepcopy(self.prov_data.get_provisioner('beaker').rgs[0])
        host = resource_group.recipeset.hosts[0]

        # Keep tasks from index 2.
        host.rerun_recipe_tasks_from_index = 2
        wrap = ShellWrap(resource_group, **{'keycheck': 'no'})
        xml_content = """
        <job>
          <recipeSet>
            <recipe id="123" ks_meta="harness='restraint-rhts beakerlib'">
              <task name="/distribution/check-install" role="STANDALONE">
                <params>
                  <param name="CKI_NAME" value="/distribution/check-install"/>
                  <param name="CKI_UNIVERSAL_ID" value="/distribution/check-install"/>
                </params>
              </task>
            </recipe>
            <recipe id="456" ks_meta="harness='restraint-rhts beakerlib'">
              <task name="TestUsedToCheckTestPlanLogic1" role="STANDALONE">
                <params>
                  <param name="CKI_NAME" value="TestUsedToCheckTestPlanLogic1"/>
                  <param name="CKI_UNIVERSAL_ID" value="TestUsedToCheckTestPlanLogic1"/>
                </params>
              </task>
            </recipe>
            <recipe id="789" ks_meta="harness='restraint-rhtsbeakerlib'">
              <task name="TestUsedToCheckTestPlanLogic2" role="STANDALONE">
                <params>
                  <param name="CKI_NAME" value="TestUsedToCheckTestPlanLogic2"/>
                  <param name="CKI_UNIVERSAL_ID" value="TestUsedToCheckTestPlanLogic2"/>
                </params>
              </task>
            </recipe>
          </recipeSet>
        </job>
        """
        expected = remove_spaces_and_new_lines(
            ET.canonicalize(
                ET.tostring(
                    ET.fromstring(xml_content), encoding='unicode')
            )
        )
        result = remove_spaces_and_new_lines(str(wrap.run_restraint_job))

        self.assertEqual(expected, result)

    def test_update_duration(self):
        """Ensure update_duration computes sets 120% of duration of tasks."""
        host = Host({'recipe_id': 1234})

        restraint_recipe = RestraintRecipe.create_from_string('''<recipe>
            <task name="a1" status="Aborted">
                <params>
                    <param name="KILLTIMEOVERRIDE" value="10" />
                </params>
            </task>
            <task name="/distribution/command" status="Cancelled">
                <params>
                    <param name="KILLTIMEOVERRIDE" value="20" />
                </params>
            </task>
        </recipe>''')
        ShellWrap.update_duration(host, restraint_recipe.tasks)

        # 120% of 30 is 36.
        self.assertEqual(host.duration, 36)

    def test_update_duration_default(self):
        """Ensure update_duration uses a hardcoded default if tasks don't have duration set."""
        host = Host({'recipe_id': 1234, 'duration': 0})
        ShellWrap.update_duration(host, [])
        # Default is set.
        self.assertEqual(host.duration, const.DEFAULT_RESERVESYS_DURATION)
