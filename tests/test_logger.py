"""Test cases for logger module."""
import unittest
from unittest import mock

from upt.logger import COLORS
from upt.logger import LOGGER


class TestLogger(unittest.TestCase):
    # pylint: disable=no-self-use
    """Test cases for logger module."""

    @mock.patch('builtins.print')
    def test_printc(self, mock_print):
        """Ensure printc works."""
        color = COLORS.BLUE
        data = 'test'
        LOGGER.printc(data, color)
        mock_print.assert_called_with(f'{color}{data}{COLORS.RESET}')

    @mock.patch('builtins.print')
    def test_print_result_in_color(self, mock_print):
        """Ensure print_result_in_color works."""
        LOGGER.print_result_in_color('test Warn WARN FAIL fail')
        mock_print.assert_called_with(f'test {COLORS.YELLOW}Warn{COLORS.RESET} '
                                      f'{COLORS.YELLOW}WARN{COLORS.RESET} '
                                      f'{COLORS.RED}FAIL{COLORS.RESET} {COLORS.RED}fail'
                                      f'{COLORS.RESET}')

    @mock.patch('builtins.print')
    def test_print_result_in_color2(self, mock_print):
        """Ensure test_print_result_in_color works with difficult case."""
        data = 'R:1 is done processing [mark_recipe_done]'
        LOGGER.print_result_in_color(data)
        mock_print.assert_called_with(f'R:1 is {COLORS.GREEN}done{COLORS.RESET} processing'
                                      f' [mark_recipe_done]')
