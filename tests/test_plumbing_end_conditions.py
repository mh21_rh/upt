"""Test cases for end_conditions module."""
import unittest
from unittest import mock

from upt.plumbing.end_conditions import ProvEndConds
from upt.plumbing.objects import ResourceGroup


class TestEndConditions(unittest.TestCase):
    """Test cases for ProvEndConds class."""

    def setUp(self) -> None:
        self.noop = lambda x: x

    @mock.patch('builtins.print', mock.Mock())
    def test_abort_count_exceeded(self):
        """Ensure we don't reprovision when we run out of attempts."""
        rg_out_of = ResourceGroup({'erred_rset_ids': {0, 1, 2}})
        prov_update = mock.Mock()
        prov_reprovision = mock.Mock()
        cond = ProvEndConds(rg_out_of, prov_update, prov_reprovision, self.noop, self.noop,
                            self.noop)
        # Run .evaluate()
        cond.evaluate(False, max_aborted_count=3)
        # We need to update data each time
        prov_update.assert_called()
        # However, we've exceeded number of aborts len(erred_rset_ids) == 3, so
        # reprovision_aborted must not be called.
        prov_reprovision.assert_not_called()

    @mock.patch('builtins.print', mock.Mock())
    def test_end_provisioning_success(self):
        """Ensure resource groups with provisioning_done set to True will stop execution."""
        cond = ProvEndConds(ResourceGroup(), self.noop, self.noop, self.noop, self.noop, self.noop)
        result = cond.evaluate(False, max_aborted_count=3, time_cap=False)
        self.assertTrue(result)

    def test_end_provisioning_fail(self):
        """Ensure resource groups with provisioning_done set to False will keep waiting."""
        rg_not_done = ResourceGroup()
        cond = ProvEndConds(rg_not_done, lambda x: False, self.noop, self.noop, self.noop, 100)
        result = cond.evaluate(False, max_aborted_count=3, time_cap=False)
        self.assertFalse(result)

    @mock.patch('upt.plumbing.end_conditions.time.time')
    @mock.patch('upt.logger.LOGGER.error')
    def test_timeout_timecap(self, mock_error, mock_time):
        """Ensure time_cap timeout works (1000s limit)."""

        rg_not_done = ResourceGroup({'resource_id': 'J:1234'})
        cond = ProvEndConds(rg_not_done, lambda x: False, self.noop, self.noop, self.noop, 100)
        result = cond.timeout(**{'time_cap': 1000})
        self.assertFalse(result)

        mock_time.side_effect = [1, 5]
        result = cond.timeout(**{'time_cap': 1})
        self.assertTrue(result)
        mock_error.assert_called_with('Provisioning of %s canceled after %s minutes.', 'J:1234', 1)
        result = cond.timeout(**{'time_cap': 1})
        self.assertTrue(result)
