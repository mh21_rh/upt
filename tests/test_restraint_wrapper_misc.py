"""Test case from upt.restraint.wrapper.misc module."""
import pathlib
import unittest

from upt.restraint.wrapper import misc

from . import utils


class TestRestraintMisc(unittest.TestCase):
    """Test cases for functions related to TaskResult."""

    def test_clean_filename_encoded_as_html(self):
        """Test fix_file_name_encoded_as_html."""
        cases = (
            ('File name without HTML characters', 'El niño.txt', 'El niño.txt'),
            ('File name with HTML characters', 'El%20ni%C3%B1o.txt', 'El niño.txt')
        )
        for description, original_file, expected_file in cases:
            with self.subTest(description), utils.create_temporary_files([original_file]):
                final_file = misc.fix_file_name_encoded_as_html(pathlib.Path(original_file))
                self.assertEqual(final_file.name, expected_file)

    def test_create_restraint_log(self):
        """Test create_restraint_log."""
        with utils.create_temporary_files([]):
            log = pathlib.Path('dir1/dir2/file.txt')
            content = 'some content'
            relative_path = pathlib.Path('dir1')

            restraint_log = misc.create_restraint_log(log, content, relative_path)

            self.assertTrue(log.is_file())

            self.assertEqual(f'dir2/{log.name}', restraint_log.path)
            self.assertEqual(log.name, restraint_log.filename)
